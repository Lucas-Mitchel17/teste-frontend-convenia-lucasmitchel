import CalcDataSection 		from './CalcData.section';
import ResultsSection 	from './Results.section';

export {
	CalcDataSection,
	ResultsSection,
};
