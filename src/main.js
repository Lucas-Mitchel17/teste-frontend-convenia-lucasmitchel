import Vue from 'vue';
import App from './App.vue';
import store from './store';
import './styles/style.sass';

import apolloProvider from './apollo';

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.config.productionTip = false;

new Vue({
  store,
  apolloProvider,
  render: h => h(App)
}).$mount('#app');
