import Vue from 'vue';
import Vuex from 'vuex';
import { apolloClient } from '../apollo';
import { GetCurrenciesQuery } from '@/graphql';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		totalValue: 0,
		tip: 0.10,
		people: 2,
		currencyBRL: 0,
		currencies:{
			dolar: 0,
			euro: 0
		},
		currencyType: 'dolar',
	},
	mutations: {
		setPeople: (state, data) => state.people = data,
		setTotalValue: (state, data) => state.totalValue = data,
		setTip: (state, data) => state.tip = data,
		setCurrencyBRL: (state, data) => state.currencyBRL = data,
		setCurrencies: (state, data) => state.currencies = data,
		setCurrencyType: (state, data) => state.currencyType = data,
	},
	actions: {
		async loadCurrencies({ commit }) {
			await apolloClient.query({
				query: GetCurrenciesQuery
			}).then(({ data }) => {
				const currencyParams = {
					dolar: data.dolar[0].quoteAmount,
					euro: data.euro[0].quoteAmount
				};

				commit('setCurrencies', currencyParams);
			}).catch(() => {
        alert('Desculpe! Houve um erro ao carregar a aplicação. Por favor recarregue a página!');
      });
		}
	}
});
